# API 

## Quickstart 

### Initialisation 

- create a virtual environment using: 
venv/scripts/activate
- activate it 
- install the requirements: 
pip install -r requirements.txt

### Run 

- you can run the application using: 
uvicorn:main:app

## Contenu

Cette application permet de créer un webservice local qui:
- récupère un deck de l'api https://deckofcardsapi.com/ et renvoie son identifiant via une requête GET sur /creer-un-deck
- tire x cartes du deck via une requête POST sur /cartes/{nombre_carte} avec un body {deck_id: str}
