from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel

import requests

import json

from configuration import get_configuration

app = FastAPI()

class Deck(BaseModel):
    id:str

@app.get("/creer-un-deck/")
async def add_deck():
    request_deck = requests.get('https://deckofcardsapi.com/api/deck/new/')
    return request_deck.json()["deck_id"]

@app.post("/cartes/{nombre_cartes}")
async def create_carte(nombre_cartes: int, q: Optional[str]= None):
    request_deck_id = requests.get('https://deckofcardsapi.com/api/deck/new/').json()["deck_id"]
    parameters = {'count':nombre_cartes}
    url = 'https://deckofcardsapi.com/api/deck/' + request_deck_id + '/draw/'
    request_carte = requests.get(url, params = parameters).json()
    return {"cartes" :request_carte, "q": q}


if __name__ == "__main__":
    r = requests.get('https://deckofcardsapi.com/api/deck/new/')
    print(r.status_code)
