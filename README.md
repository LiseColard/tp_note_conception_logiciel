# TP noté: Conception Logiciel 

## Présentation du projet

### Introduction 

Ce projet met en place un webservice et un client du webservice dans les deux fichiers correspondant. La partie webservice et la partie client sont considérées comme des modules applicatifs indépendants. 

### Scénario

Extrait du sujet: "En utilisant les fonctions précédentes, mettre en place un scénario executable qui initialise un deck, tire 10 cartes et compte le nombre de cartes tirées de chaque couleur. "

### Quickstart

Le détail sur comment exécuter l'application sont des les ReadMe de chaque dossier (Webservice et Client). Il faut commencer par le dossier Webservice.  

