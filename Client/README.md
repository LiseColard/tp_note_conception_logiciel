# API client

## Quickstart 

### Initialisation 

- Create a virtual environment 
- Activate it: 
venv/scripts/activate
- install the requirements: 
pip install -r requirements.txt
- copy .env.local values in .env 

### Run 
- run the code: 
python main.py 

## Contenu 

Un client pour le webservice crée précédemment peut: 
- initialiser un deck par requête HTTP GET 
- tirer des cartes du deck en cours via une requête HTTP POST
- calculer pour une liste de carte donnée en entrée le nombre de cartes de chaque couleur sous forme d'un dictionnaire. 
