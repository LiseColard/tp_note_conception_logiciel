from dotenv import dotenv_values

def get_configuration():
    return dotenv_values(".env")
