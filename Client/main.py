import requests
import json
from configuration import get_configuration

api_url = get_configuration()

class Deck:
    id:str
    def __init__(self,id):
        self.id=id

def get_id():
    r= requests.get(api_url + "/creer-un-deck/")
    return r 

deck = Deck(id = get_id())

def tirer_carte(nombre_carte):
    r = requests.post(api_url + "/cartes/" + str(nombre_carte))
    return r

def nombre_carte_par_couleur(liste_carte):
    H=0
    S=0
    D=0
    C=0
    for cards in liste_carte["cards"]:
        if cards["suit"] == "SPADES":
            S+=1
        elif cards["suit"] == "DIAMONDS":
            D+=1
        elif cards["suit"] == "CLUBS":
            C+=1
        elif cards["suit"] == "HEARTS":
            H+=1
    sortie = {"H":H,"S":S,"D":D,"C":C}
    return sortie

if __name__ == "__main__":
    r= get_id()
    test = tirer_carte(6)
    
